const { connect } = require('http2')
const mysql = require('mysql2')

const openConnection = (()=>{
    const connection = mysql.createConnection({
        // port : 3306,
        // host: 'db',
        uri: "mysql://db:3306",
        user: 'root',
        password:'root',
        database : 'assignment'

    })
    
})

module.exports = {
    openConnection
}
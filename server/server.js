const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors("*"));

const routerEmp = require('./routes/emp');
app.use(express.json())

app.use('/emp',routerEmp);

app.listen(4000, "0.0.0.0", ()=> {
    console.log('server started on 4000')
})


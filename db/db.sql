create table Emp (
    empid int primary key auto_increment,
    name varchar(100),
    salary decimal(10,2),
    age int
);